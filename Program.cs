﻿using System;
using System.Collections.Generic;

namespace LV5
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IShipable> ProductList = new List<IShipable>();
            Box box = new Box("1. box");
            ProductList.Add(new Product("1. porduct", 3, 50));
            ProductList.Add(new Product("2. porduct", 5, 15));
            ProductList.Add(new Product("3. porduct", 10, 100));
            box.Add(new Box("1. box"));
            ShippingService service = new ShippingService(1.5);
            foreach (IShipable product in ProductList) 
            {
                box.Add(product);
            }
            double totalPrice = box.Price + service.CalculateDeliveryPrice(box);
            Console.WriteLine("Total weight: " + box.Weight);
            Console.WriteLine("Product price: " + box.Price);
            Console.WriteLine("Delivery price: " + service.CalculateDeliveryPrice(box));
            Console.WriteLine("Total price: " + totalPrice);
        }
    }
}
