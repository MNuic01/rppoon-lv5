﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV5
{
    class ShippingService
    {
        private double unitprice;
        public ShippingService(double unitprice)
        {
            this.unitprice = unitprice;
        }
        public double CalculateDeliveryPrice(Box item) 
        {
            double deliveryPrice = 0;
            deliveryPrice = item.Weight * unitprice;
            return deliveryPrice;
        }
    }
}
